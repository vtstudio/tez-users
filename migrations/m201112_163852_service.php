<?php

use yii\db\Migration;

/**
 * Class m201112_163852_service
 */
class m201112_163852_service extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('service', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255),
            'price' => $this->float(2)->defaultValue(0),
            'sort' => $this->integer(11)->defaultValue(0),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),

        ], $tableOptions);


        $this->createIndex(
            'idx-service-sort',
            'service',
            'sort'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201112_163852_service cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201112_163852_service cannot be reverted.\n";

        return false;
    }
    */
}
