<?php

use yii\db\Migration;

/**
 * Class m201113_145932_customer_address
 */
class m201113_145932_customer_address extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('customer_address', [
            'id' => $this->primaryKey(),
            'customer_id' => $this->integer(11)->null(),

            'country_id' => $this->integer(11)->null(),
            'city_id' => $this->integer(11)->null(),

            'phone' => $this->bigInteger(12)->defaultValue(0),
            'email_user' => $this->string()->defaultValue(''),

            'default' => $this->smallInteger(1)->notNull()->defaultValue(0),
            'sort' => $this->integer(11)->defaultValue(0),

            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),

        ], $tableOptions);


        $this->createIndex(
            'idx-customer_address-sort',
            'customer_address',
            'sort'
        );

        $this->addForeignKey(
            'idx-customer_address_2_customer',
            'customer_address',
            'customer_id',
            'customer',
            'id',
            'SET NULL'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201113_145932_customer_address cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201113_145932_customer_address cannot be reverted.\n";

        return false;
    }
    */
}
