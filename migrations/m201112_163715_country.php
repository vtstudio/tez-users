<?php

use yii\db\Migration;

/**
 * Class m201112_163715_country
 */
class m201112_163715_country extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('country', [
            'id' => $this->primaryKey(),

            'sort' => $this->integer(11)->defaultValue(0),
            'title' => $this->string(255),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),

        ], $tableOptions);


        $this->createIndex(
            'idx-country-sort',
            'country',
            'sort'
        );


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201112_163715_Country cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201112_163715_Country cannot be reverted.\n";

        return false;
    }
    */
}
