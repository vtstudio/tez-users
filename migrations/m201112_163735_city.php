<?php

use yii\db\Migration;

/**
 * Class m201112_163735_city
 */
class m201112_163735_city extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('city', [
            'id' => $this->primaryKey(),
            'country_id' => $this->integer(11)->null(),
            'title' => $this->string(255),
            'sort' => $this->integer(11)->defaultValue(0),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),

        ], $tableOptions);


        $this->createIndex(
            'idx-city-sort',
            'city',
            'sort'
        );

        $this->addForeignKey(
            'idx-city_2_country',
            'city',
            'country_id',
            'country',
            'id',
            'SET NULL'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201112_163735_city cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201112_163735_city cannot be reverted.\n";

        return false;
    }
    */
}
