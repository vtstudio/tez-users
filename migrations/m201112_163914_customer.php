<?php

use yii\db\Migration;

/**
 * Class m201112_163914_customer
 */
class m201112_163914_customer extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('customer', [
            'id' => $this->primaryKey(),
            'gender_id' => $this->integer(11)->null(),
            'first_name' => $this->string(255)->notNull(),
            'last_name' => $this->string(255)->notNull(),

            'country_id' => $this->integer(11)->null(),
            'city_id' => $this->integer(11)->null(),

            'birth_date' => $this->date()->null(),
            'phone' => $this->bigInteger(12)->defaultValue(0),
            'email_user' => $this->string()->defaultValue(''),

            'metaphone_first_name' => $this->string(255)->null(),
            'metaphone_last_name' => $this->string(255)->null(),

            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),

        ], $tableOptions);


        $this->createIndex(
            'idx-customer-metaphone_first_name',
            'customer',
            'metaphone_first_name'
        );

        $this->createIndex(
            'idx-customer-metaphone_last_name',
            'customer',
            'metaphone_last_name'
        );

        $this->addForeignKey(
            'idx-customer_2_country',
            'customer',
            'country_id',
            'country',
            'id',
            'SET NULL'
        );

        $this->addForeignKey(
            'idx-customer_2_city',
            'customer',
            'city_id',
            'city',
            'id',
            'SET NULL'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201112_163914_customer cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201112_163914_customer cannot be reverted.\n";

        return false;
    }
    */
}
