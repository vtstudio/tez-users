<?php

use yii\db\Migration;

/**
 * Class m201112_164009_order_item
 */
class m201112_164009_order_item extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('order_item', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(11)->null(),
            'service_id' => $this->integer(11)->null(),

            'sum' => $this->float(2)->defaultValue(0),
            'created_at' => $this->dateTime()->notNull(),
            'updated_at' => $this->dateTime()->notNull(),

        ], $tableOptions);


        $this->addForeignKey(
            'idx-order_item_2_order',
            'order_item',
            'order_id',
            'order',
            'id',
            'SET NULL'
        );

        $this->addForeignKey(
            'idx-order_item_2_service',
            'order_item',
            'service_id',
            'service',
            'id',
            'SET NULL'
        );


    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m201112_164009_order_item cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m201112_164009_order_item cannot be reverted.\n";

        return false;
    }
    */
}
