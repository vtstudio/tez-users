<?php

namespace app\controllers;

use app\components\MyHelper;
use app\models\CompareCreate;
use app\models\UpdateCustomer;
use Yii;
use app\models\Customer;
use app\models\search\CustomerSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;

/**
 * CustomerController implements the CRUD actions for Customer model.
 */
class CustomerController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Customer models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new CustomerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }
    public function actionIndexAjax()
    {
        if (!Yii::$app->request->isAjax)
        {

            $get = Yii::$app->request->queryParams;

            return $this->redirect(['index']);
        }
        $searchModel = new CustomerSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->renderAjax('index-ajax', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }

    /**
     * Displays a single Customer model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Customer model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */



    public function actionCreate()
    {
        $model = new Customer();

        if (Yii::$app->request->post()) {

            $post = Yii::$app->request->post();

            $data = new CompareCreate();
            $data->first_name = $post['Customer']['first_name'];
            $data->last_name = $post['Customer']['last_name'];
            $data->email_user = $post['Customer']['email_user'];
            $data->phone = $post['Customer']['phone'];
            $data->gender_id = $post['Customer']['gender_id'];
            $data->country_id = $post['Customer']['country_id'];
            $data->city_id = $post['Customer']['city_id'];
            $data->birth_date = $post['Customer']['birth_date'];

            $result = $data->insertCustomer();

            $id = Url::to(['customer/update', 'id' => $result[1]]);
            if ($result[0] == 'create'){
                Yii::$app->session->setFlash("success", Yii::t('app', "Новый клиент добавлен"));
            }else{
                Yii::$app->session->setFlash("success", Yii::t('app', "Такой клиент уже существует, данные обновлены"));
            }

            return $id;
        }

        return $this->renderPartial('_form', [
            'model' => $model,
        ]);
    }


    /**
     * Updates an existing Customer model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $data = new CompareCreate();
            $data->first_name = $model->first_name;
            $data->last_name = $model->last_name;
            $data->gender_id = $model->gender_id;
            $data->metaphone_first_name = metaphone(MyHelper::translit($model->first_name));
            $data->metaphone_last_name = metaphone(MyHelper::translit($model->last_name));

            $compare = $data->compareCustomer();

            if ($compare != $model->id){
                $updateC = new UpdateCustomer();
                $updateC->idNewUser = $compare;
                $updateC->idOldUser = $model->id;
                $updateC->start();

                Yii::$app->session->setFlash("warning", Yii::t('app', "Все данные успешно перенесены"));
                return $this->redirect(['update', 'id' => $compare]);
            }else{
                Yii::$app->session->setFlash("success", Yii::t('app', "Данные обновлены"));
                return $this->redirect(['update', 'id' => $compare]);
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionCompare()
    {
        if (!Yii::$app->request->isAjax) return false;

        $data = Yii::$app->request->post();

        $compare = new CompareCreate();
        $compare->first_name = $data['first_name'];
        $compare->last_name = $data['last_name'];
        $compare->gender_id = $data['gender_id'];
        $compare->metaphone_first_name = metaphone(MyHelper::translit($data['first_name']));
        $compare->metaphone_last_name = metaphone(MyHelper::translit($data['last_name']));
        $res = $compare->compareCustomer();
        return json_encode($res);
    }

    public function actionCits()
    {

        if (!Yii::$app->request->isAjax) return true;
        $post = Yii::$app->request->post();
        $model = new Customer();
        $data = $model->getCityV($post['id']);
        return json_encode($this->renderAjax('_city', [
            'data' => $data,
        ]));
    }

    public function actionSearch()
    {

        if (!Yii::$app->request->isAjax) return true;
        $post = Yii::$app->request->post();
        $result = [];
        switch ($post['wey']){
            case "name":
                $r = Customer::find()
                    ->where([ 'like', 'first_name', $post['value']])
                    ->orWhere(['like', 'metaphone_first_name', metaphone(MyHelper::translit($post['value']))])
                    ->asArray()->all();
                if (!empty($r)) {
                    foreach ($r as $key => $value){
                        $result[$key] = [
                            'label' => "{$value['first_name']} ({$value['last_name']})",
                            'value' => $value['first_name'],
                        ];
                    }
                }
                break;

            case "last":
                $r = Customer::find()
                    ->where([ 'like', 'last_name', $post['value']])
                    ->orWhere(['like', 'metaphone_last_name', metaphone(MyHelper::translit($post['value']))])
                    ->asArray()
                    ->all();
                if (!empty($r)) {
                    foreach ($r as $key => $value){
                        $result[$key] = [
                            'label' => "{$value['last_name']} ({$value['first_name']})",
                            'value' => $value['last_name'],
                        ];
                    }
                }
                break;

            default:
                $result = [];
                break;
        }

        return json_encode($result);
    }






    /**
     * Deletes an existing Customer model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash("success", Yii::t('app', "Удалено"));
        return $this->redirect(['index']);
    }

    /**
     * Finds the Customer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Customer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Customer::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
