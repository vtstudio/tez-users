<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

$customer = $model->customer;


$this->title = "Создание заказа клиента: {$customer->first_name} {$customer->last_name}";
$this->params['breadcrumbs'][] = ['label' => 'Клиенты', 'url' => ['customer/index']];
$this->params['breadcrumbs'][] = ['label' => 'Клиент: ' . " {$customer->first_name} {$customer->last_name}", 'url' => ['customer/update', 'id' => $model->customer_id]];
$this->params['breadcrumbs'][] = ['label' => "Заказы клиента: {$customer->first_name} {$customer->last_name}", 'url' => ['index', 'customer_id' => $model->customer_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
