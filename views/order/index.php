<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

    $customer = $searchModel->customer;

$this->title = "Заказы клиента: {$customer->first_name} {$customer->last_name}";
$this->params['breadcrumbs'][] = ['label' => 'Клиенты', 'url' => ['customer/index']];
$this->params['breadcrumbs'][] = ['label' => 'Клиент: ' . " {$customer->first_name} {$customer->last_name}", 'url' => ['customer/update', 'id' => $searchModel->customer_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <h3>Сумарный доход: <b><?=$searchModel->customer->getOrdersSum()?></b></h3>
    <p>
        <?= Html::a('Создать заказ', ['order/create', 'customer_id' => $searchModel->customer_id], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],


            ['attribute' => 'created_at', 'value' => function ($data) {
                $date = "";
                if ($data->created_at) {
                    $date = date('Y-m-d', strtotime($data->created_at));
                }
                return $date;
            }, 'headerOptions' => ['width' => '250'], 'filter' => \kartik\daterange\DateRangePicker::widget([
                'model' => $searchModel,
                'attribute' => 'created_at',

                'convertFormat' => true,
                'pluginOptions' => [
                    'locale' => ['format' => 'Y-m-d'],
                ]
            ])],

            ['attribute' => 'items', 'value' => function($data){
                return $data->setItemsArr();
            }, 'format' => 'raw'],


            ['attribute' => 'sum', 'value' => function($data){
                return "{$data->sum} USD";
            }],


            ['class' => 'yii\grid\ActionColumn',
                'template' => "{update} {delete}",
                'headerOptions' => ['width' => '50'],
            ]
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
