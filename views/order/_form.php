<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">
    <div class="row">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'items')->widget(\kartik\select2\Select2::class, [
            'data' => $model->getItems(),
            'value' => $model->items,
            'options' => ['multiple' => true, 'placeholder' => 'Выбор услуг'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ])?>

        <div id="items"></div>

        <div class="form-group">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$urlItem = Url::to(['order/getItem']);

$js = <<<JS



JS;
$this->registerJs($js, yii\web\View::POS_END);
?>