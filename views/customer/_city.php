<?php
?>

<div class="form-group field-customer-city_id has-success">
    <label class="control-label" for="customer-city_id">Город</label>
    <select id="customer-city_id" class="form-control" name="Customer[city_id]" aria-invalid="false">
        <?php foreach ($data as $key => $value):?>
            <option value="<?=$key?>"><?=$value?></option>
        <?php endforeach;?>
    </select>

    <div class="help-block"></div>
</div>
