<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

$countries = $model->getCountryAll();
/* @var $this yii\web\View */
/* @var $model app\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-form">

    <?php $form = ActiveForm::begin([
        'id' => 'customer-form',
        'action' => \yii\helpers\Url::to(['customer/create']),
        'method' => 'post',
        'options' => ['autocomplete' => false]
    ]); ?>

    <div class="row">
        <div class="tab-content">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row">

                    <div class="col-sm-3">
                        <?= $form->field($model, 'first_name')->textInput(['autocomplete' => 'off', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-3">
                        <?= $form->field($model, 'last_name')->textInput(['autocomplete' => 'off', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-3">
                        <?= $form->field($model, 'email_user')->widget(MaskedInput::className(), ['mask' => '*{3,20}@*{3,10}.*{2,5}'])->textInput(['placeholder' => $model->getAttributeLabel('email_user')]); ?>
                    </div>
                    <div class="col-sm-3">
                        <?= $form->field($model, 'phone')->widget(MaskedInput::className(), ['mask' => '+99(999)999 99 99'])->textInput(['placeholder' => $model->getAttributeLabel('phone')]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        <?= $form->field($model, 'gender_id')->dropDownList($model->getGenders()) ?>
                    </div>
                    <div class="col-sm-3">
                        <?= $form->field($model, 'country_id')->dropDownList($countries) ?>
                    </div>
                    <div class="col-sm-3" id="cits">
                        <?= $form->field($model, 'city_id')->dropDownList($model->getCityV(array_key_first($countries))) ?>
                    </div>
                    <div class="col-sm-3">
                        <?= $form->field($model, 'birth_date')->widget(\kartik\daterange\DateRangePicker::classname(), [
                            'pluginOptions'=>[
                                'locale' => ['format' => 'Y-MM-DD'],
                                'singleDatePicker'=>true,
                                'showDropdowns'=>true,
                            ],
                            'options' => ['placeholder' => 'Выбрать даты...']
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success', 'id' => 'customer-form-s']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
