<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use yii\helpers\Url;

use \app\components\MyHelper;

use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CustomerSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Клиенты';
$this->params['breadcrumbs'][] = $this->title;
?>
    <style>
        ul.ui-autocomplete {
            z-index: 1100;
        }
    </style>
    <div class="customer-index">

        <h1><?= Html::encode($this->title) ?></h1>


<!--        --><?php //Pjax::begin(); ?>
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
        <p>
            <?= Html::a('Добавить клиента', ['create'], ['class' => 'btn btn-success', 'data-toggle' => "modal", "data-target" => "#modal"]) ?>
        </p>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                ['attribute' => 'gender_id', 'value' => function ($data) {
                    return $data->getGender();
                }, 'filter' => false, 'headerOptions' => ['width' => 100]],


                ['attribute' => 'first_name', 'value' => function ($data) {
                    return Html::a($data->first_name, ['customer/update', 'id' => $data->id]);
                }, 'filter' => false, 'format' => 'raw', 'headerOptions' => ['width' => 130]],

                ['attribute' => 'last_name', 'value' => function ($data) {
                    return Html::a($data->last_name, ['customer/update', 'id' => $data->id]);
                },'filter' => false, 'format' => 'raw', 'headerOptions' => ['width' => 130]],


                ['attribute' => 'country_id', 'value' => function ($data) {

                    return $data->customerAddressOne->country['title'];
                }, 'filter' => false, 'headerOptions' => ['width' => 130]],


                ['attribute' => 'city_id', 'value' => function ($data) {
                    return $data->customerAddressOne->city['title'];
                }, 'filter' => false, 'headerOptions' => ['width' => 130]],


                ['attribute' => 'birth_date', 'value' => function ($data) {
                    return $data['birth_date'];
                }, 'filter' => false, 'format' => ['date', 'dd.MM.Y'], 'headerOptions' => ['width' => 100]],


                ['attribute' => 'phone', 'value' => function ($data) {
                    return MyHelper::phoneFormatter($data->customerAddressOne['phone']);
                }, 'filter' => false, 'headerOptions' => ['width' => 250]],

                ['attribute' => 'email_user', 'value' => function ($data) {
                    return $data->customerAddressOne['email_user'];
                }, 'filter' => false, 'headerOptions' => ['width' => 100]],

                ['attribute' => 'qty_orders','value' => function($data){
                    return count($data->orders);
                },'filter' => false,'headerOptions' => ['width' => 30]],

                ['attribute' => 'total_income', 'value' => function($data){
                    $res = $data->getOrdersSum();
                    return $res ? $res : 0;
                }, 'filter' => false,'headerOptions' => ['width' => 50]],

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{prewOrder} {addOrder} {update} {delete}',
                    'buttons' => [

                        'prewOrder' => function ($url, $model, $key) {
                            return Html::a('', ['order/index', 'customer_id' => $model->id, 'page' => Yii::$app->request->get('page')],
                                [
                                    'class' => 'glyphicon glyphicon-shopping-cart',
                                    'title' => \Yii::t('yii', 'Просотр заказов')
                                ]);
                        },
                        'addOrder' => function ($url, $model) {
                            return Html::a('', ['order/create', 'customer_id' => $model->id],
                                [
                                    'class' => 'glyphicon glyphicon-plus',
                                    'title' => \Yii::t('yii', 'Добавить заказ')
                                ]);
                        }
                    ]
                ],
            ],
        ]); ?>

<!--        --><?php //Pjax::end(); ?>


        <?php
        Modal::begin([
            'header' => 'Добавить клиента',
            'id' => 'modal',
            'size' => 'modal-lg',
        ]);
        ?>
        <div id='modal-content'
        <div class="customer-form">

            <?= Yii::$app->runAction('customer/create', ['option_id' => $model->id]) ?>

        </div>
    </div>
<?php Modal::end(); ?>

    </div>
<?php

$create = Url::to(['customer/create']);
$cits = Url::to(['customer/cits']);
$search = Url::to(['customer/search']);


$js = <<<JS
    $("body").keydown("input", function(e) {
        if(e.keyCode === 13) {
          e.preventDefault();
        }
    });
    
    $("body").on("input", "#customer-first_name,#customersearch-first_name,#customer-last_name,#customersearch-last_name",function(e){
        
        if(e.keyCode === 13) {
           e.preventDefault();
        }
        
        let idInput = "#" + $(this).attr('id') + "";
        
        let wey = '';
        if (idInput == "#customer-first_name" || idInput == "#customersearch-first_name"){
            wey = "name";
        }else {
            wey = "last";
        }
        
        
        let value = $(e.target).val();
        let data = {value:value, wey:wey};
        
        data[yii.getCsrfParam()] = yii.getCsrfToken();

        if (value.length >= 2){
            $.ajax({
            url: "{$search}",
            data: data,
            type: 'POST',
            context: $(this),
            success: function (data) {
                if (!data) {
                    console.log('not data');
                }
                let views = JSON.parse(data);
                $("#itemsSearch").html(views);
                $(idInput).autocomplete({
                  source: views
                });  
            },
            error: function () {
                console.log('Что то пошло не так');
            }
            });
           
        }
    });
    
     

    $("body").on('change', "#customer-country_id", function() {
        var id = $(this).val();
        var data = {id:id};
        data[yii.getCsrfParam()] = yii.getCsrfToken();
        $.ajax({
            url: "{$cits}",
            data: data,
            type: 'POST',
            context: $(this),
            success: function (data) {
                if (!data) {
                    console.log('not data');
                }
                let views = JSON.parse(data);
                $("#cits").html(views);
                
            },
            error: function () {
                console.log('Что то пошло не так');
            }
        });
    });

    $("body").on('click', "#customer-form-s", function(e) {
        var data = {};
        $('#customer-form').find ('input, textearea, select').each(function() {
          data[this.name] = $(this).val();
        });
        
        if (data["Customer[first_name]"].length != 0 || data["Customer[last_name]"].length != 0){
            e.preventDefault();
            data[yii.getCsrfParam()] = yii.getCsrfToken();
            
            
            $.ajax({
            url: "{$create}",
            data: data,
            type: 'POST',
            context: $(this),
            success: function (data) {
                if (!data) {
                    console.log('not data');
                }
                // let views = JSON.parse(data);
                // $('#modal').modal('hide');
                // $("form#customer-form").trigger('reset');
                
                window.location.href=data
                // console.log(data);
                // $(".content").html(data);
                
            },
            error: function () {
                console.log('Что то пошло не так');
            }
        });
            
            
           
            
        }

    });

JS;
$this->registerJs($js, yii\web\View::POS_END);
?>