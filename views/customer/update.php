<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Customer */

$this->title = 'Клиент: ' . $model->first_name . ' ' . $model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Клиенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
    <style>
        #alert {
            display: none;
        }
    </style>
    <div class="alert alert-warning" id="alert" role="alert"></div>
<div class="customer-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <h3>Сумарный доход: <b><?=$model->getOrdersSum()?></b></h3>
    <div class="btn-group btn-group-justified">
        <div class="btn-group">
            <?=Html::a('Основные данные', ['customer/update', 'id' => $model->id], ['class' => 'btn btn-default'])?>
        </div>
        <div class="btn-group">
            <?=Html::a('Заказы', ['order/index', 'customer_id' => $model->id], ['class' => 'btn btn-default'])?>
        </div>
        <div class="btn-group">
            <?=Html::a('Адреса клиента', ['customer-address/index', 'customer_id' => $model->id], ['class' => 'btn btn-default'])?>
        </div>
    </div>

    <br>
    <hr>

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="tab-content">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row">

                    <div class="col-sm-3">
                        <?= $form->field($model, 'first_name')->textInput(['autocomplete' => 'off', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-3">
                        <?= $form->field($model, 'last_name')->textInput(['autocomplete' => 'off', 'maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-3">
                        <?= $form->field($model, 'gender_id')->dropDownList($model->getGenders()) ?>
                    </div>
                    <div class="col-sm-3">
                        <?= $form->field($model, 'birth_date')->widget(\kartik\daterange\DateRangePicker::classname(), [
                            'pluginOptions'=>[
                                'locale' => ['format' => 'Y-MM-DD'],
                                'singleDatePicker'=>true,
                                'showDropdowns'=>true,
                            ],
                            'options' => ['placeholder' => 'Выбрать даты...']
                        ]);
                        ?>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success', 'id' => 'customer-form-s']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php

$url = Url::to(['customer/compare']);
$customer = Url::to(['customer/update', 'id' => '']);
$js = <<<JS
    $("body").keydown("input", function(e) {
        if(e.keyCode === 13) {
          e.preventDefault();
        }
    });

    $("body").on('change', ".form-control", function() {
        var id = '"{$model->id}"';
        var first_name = $("#customer-first_name").val();
        var last_name = $("#customer-last_name").val();
        var gender_id = $("#customer-gender_id").val();
        var urlCustomer = "{$customer}";
        
        
        var data = {first_name:first_name, last_name:last_name, gender_id:gender_id};
        data[yii.getCsrfParam()] = yii.getCsrfToken();
        $.ajax({
            url: "{$url}",
            data: data,
            type: 'POST',
            context: $(this),
            success: function (data) {
                if (!data) {
                    console.log('not data');
                }
                let views = JSON.parse(data);
                
                if (data != 'false' && data != id){
                    
                    let text = 'Внимание клиент с такими данными существует, в случае сохранения все данные текущего клиента, перенесутся к существующему. ';
                    text += '<p><b><a href=' + urlCustomer + views + ' target="_blank">Посмотреть существующего клиента</a></b></p>';
                    $("#alert").css('display', 'block');
                    $("#alert").html(text);
                }else {
                    $("#alert").css('display', 'none');
                }
                
                
            },
            error: function () {
                console.log('Что то пошло не так');
            }
        });
        
        console.log(data);
        
    });
    
    



JS;
$this->registerJs($js, yii\web\View::POS_END);

?>