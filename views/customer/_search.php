<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\search\CustomerSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-3">
                <?= $form->field($model, 'first_name') ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'last_name') ?>
            </div>
            <div class="col-sm-3">
                <?php echo $form->field($model, 'phone') ?>
            </div>
            <div class="col-sm-3">
                <?php echo $form->field($model, 'email_user') ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <?= $form->field($model, 'gender_id')->dropDownList($model->getGenders(),['prompt' => '']) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'country_id')->dropDownList($model->getCountryAll(), ['prompt' => '']) ?>
            </div>
            <div class="col-sm-3">
                <?php echo $form->field($model, 'city_id')->dropDownList($model->getCityAll(), ['prompt' => '']) ?>
            </div>
            <div class="col-sm-3">
                <?= $form->field($model, 'birth_date')->widget(\kartik\daterange\DateRangePicker::classname(), [
                    'pluginOptions'=>[
                        'locale' => ['format' => 'Y-MM-DD'],
                        'singleDatePicker'=>true,
                        'showDropdowns'=>true,
                    ],
                    'options' => ['placeholder' => 'Выбрать даты...']
                ]);
                ?>
            </div>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Сброс', ['customer/index'], ['class' => 'btn btn-default']) ?>
        </div>
    </div>


    <?php ActiveForm::end(); ?>

</div>
