<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

$countries = $model->customer->getCountryAll();

/* @var $this yii\web\View */
/* @var $model app\models\CustomerAddress */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-address-form">

    <?php $form = ActiveForm::begin([
        'action' => \yii\helpers\Url::to(['customer-address/create', 'customer_id' => $model->customer_id]),
    ]); ?>

    <div class="row">
        <div class="tab-content">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row">

                    <div class="col-sm-6">
                        <?= $form->field($model, 'email_user')->widget(MaskedInput::className(), ['mask' => '*{3,20}@*{3,10}.*{2,5}'])->textInput(['placeholder' => $model->getAttributeLabel('email_user')]); ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'phone')->widget(MaskedInput::className(), ['mask' => '+99(999)999 99 99'])->textInput(['placeholder' => $model->getAttributeLabel('phone')]) ?>
                    </div>
                </div>
                <div class="row">
                   <div class="col-sm-3">
                        <?= $form->field($model, 'country_id')->dropDownList($countries) ?>
                    </div>
                    <div class="col-sm-3" id="cits">
                        <?= $form->field($model, 'city_id')->dropDownList($model->customer->getCityV((array_key_first($countries)))) ?>
                    </div>
                    <div class="col-sm-3">
                        <?= $form->field($model, 'default')->dropDownList(['Нет', 'Да']) ?>
                    </div>
                    <div class="col-sm-3" id="cits">
                        <?= $form->field($model, 'sort')->textInput(['type' => 'number']) ?>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
