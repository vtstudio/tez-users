<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\components\MyHelper;
use yii\widgets\MaskedInput;

use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CustomerAddressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$customer = $searchModel->customer;

$this->title = "Адреса клиента: {$customer->first_name} {$customer->last_name}";
$this->params['breadcrumbs'][] = ['label' => 'Клиенты', 'url' => ['customer/index']];
$this->params['breadcrumbs'][] = ['label' => 'Клиент: ' . " {$customer->first_name} {$customer->last_name}", 'url' => ['customer/update', 'id' => $searchModel->customer_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="customer-address-index">

        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?= Html::a('Добавить адрес', ['create'], ['class' => 'btn btn-success', 'data-toggle' => "modal", "data-target" => "#modal"]) ?>
        </p>

        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn', 'headerOptions' => ['width' => '50']],


                ['attribute' => 'phone', 'value' => function ($data) {
                    return MaskedInput::widget([
                        'name' => 'phone',
                        'mask' => '+99(999)999 99 99',
                        'value' => $data->phone,
                        'options' => ['class' => 'form-control update']
                    ]);
                },
                    'headerOptions' => ['width' => 200],
                    'format' => 'raw'
                ],

                ['attribute' => 'country_id', 'value' => function ($data) {
                    return Html::dropDownList('country_id', $data->country_id, $data->customer->getCountryAll(), [
                        'class' => 'form-control update country',
                        'address_id' => $data->id
                    ]);
                },
                    'filter' => $searchModel->customer->getCountryAll(),
                    'headerOptions' => ['width' => 130],
                    'format' => 'raw',
                ],

                ['attribute' => 'city_id', 'value' => function ($data) {
                    return Html::dropDownList('city_id', $data->city_id, $data->customer->getCityV($data->country_id), [
                        'class' => "form-control update city_row_{$data->id}",
                    ]);
                },
                    'filter' => $searchModel->customer->getCountryAll(),
                    'headerOptions' => ['width' => 130],
                    'format' => 'raw'
                ],


                ['attribute' => 'email_user', 'value' => function ($data) {

                    return MaskedInput::widget([
                        'name' => 'email_user',
                        'mask' => '*{3,20}@*{3,10}.*{2,5}',
                        'value' => $data->email_user,
                        'options' => ['class' => 'form-control update']
                    ]);
                },
                    'headerOptions' => ['width' => 200],
                    'format' => 'raw'
                ],

                ['attribute' => 'default', 'value' => function ($data) {
                    return Html::dropDownList('default', $data->default, ['Нет', 'Да'], ['class' => 'form-control update default']);
                },
                    'filter' => ['Нет', 'Да'],
                    'headerOptions' => ['width' => 130],
                    'format' => 'raw'
                ],

                ['attribute' => 'sort', 'value' => function ($data) {
                    return Html::input('number', 'sort', $data->sort, ['class' => 'form-control update']);
                },
                    'headerOptions' => ['width' => 200],
                    'format' => 'raw'
                ],


                ['class' => 'yii\grid\ActionColumn',
                    'template' => "{delete}",
                    'headerOptions' => ['width' => '50'],
                ]
            ],
        ]); ?>

        <?php Pjax::end(); ?>

    </div>


<?php
Modal::begin([
    'header' => 'Добавить адрес',
    'id' => 'modal',
    'size' => 'modal-lg',
]);
?>
    <div id='modal-content'
    <div class="customer-form">

        <?= Yii::$app->runAction('customer-address/create', ['customer_id' => $searchModel->customer_id]) ?>

    </div>
    </div>
<?php Modal::end(); ?>


<?php
$urlUpdate = Url::to(['customer-address/update']);
$cits = Url::to(['customer/cits']);

$js = <<<JS
    $("body").keydown("input", function(e) {
        if(e.keyCode === 13) {
          e.preventDefault();
        }
    });

    
    $("body").on('change', ".country,#customeraddress-country_id", function() {
        var id = $(this).val();
        console.log($(this).attr('id'));
        var data = {id:id};
        data[yii.getCsrfParam()] = yii.getCsrfToken();
        var orderId = $(this).parent().parent().attr('data-key');
        $.ajax({
            url: "{$cits}",
            data: data,
            type: 'POST',
            context: $(this),
            success: function (data) {
                if (!data) {
                    console.log('not data');
                }
                let views = JSON.parse(data);
                
                if ($(this).attr('id') == 'customeraddress-country_id'){
                    $("#customeraddress-city_id").html(views);
                }else {
                    $(".city_row_" + orderId).html(views);
                }
                
                
            },
            error: function () {
                console.log('Что то пошло не так');
            }
        });
        
    });
    

    $("body").on('change', ".update", function() {
        var id = $(this).parent().parent().attr('data-key');
        var val = $(this).val();
        var val_name = $(this).attr('name');
        var data = {id:id, val:val, name:val_name};
        data[yii.getCsrfParam()] = yii.getCsrfToken();
        $.ajax({
            url: "{$urlUpdate}",
            data: data,
            type: 'POST',
            context: $(this),
            success: function (data) {
                if (!data) {
                    console.log('not data');
                }
                if (data == true && val_name == 'default' && val == 1){
                    $('select[name=default]').val(0);
                    $(this).val(val)
                }
                console.log(data);
                // alert("Сохранено");
                
            },
            error: function () {
                console.log('Что то пошло не так');
            }
        });
        
        console.log(data);
        
    });
    
    



JS;
$this->registerJs($js, yii\web\View::POS_END);

?>