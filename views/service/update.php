<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Service */

$this->title = 'Обновить услугу: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Справочник услуг', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Обновить';
?>
<div class="service-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
