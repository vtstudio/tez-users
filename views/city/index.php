<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\CitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Города';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="city-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            ['attribute' => 'title', 'value' => function ($data) {
                return Html::a($data->title, ['update', 'id' => $data->id]);
            }, 'format' => 'raw'],

            ['attribute' => 'country_id', 'value' => function ($data) {
                return $data->country->title;
            }, 'filter' => $searchModel->getCoutries()],
            'sort',


            ['class' => 'yii\grid\ActionColumn',
                'template' => "{delete}",
                'headerOptions' => ['width' => '100'],
            ]
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
