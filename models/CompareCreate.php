<?php

namespace app\models;

/**
 * This is the model class for table "customer".
 *
 * @property int $id
 * @property int|null $gender_id
 * @property string $first_name
 * @property string $last_name
 * @property int|null $country_id
 * @property int|null $city_id
 * @property string|null $birth_date
 * @property int|null $phone
 * @property string|null $email_user
 * @property string|null $metaphone_first_name
 * @property string|null $metaphone_last_name
 * @property string $created_at
 * @property string $updated_at
 *
 * @property City $city
 * @property Country $country
 * @property Order[] $orders
 */
class CompareCreate extends Customer
{

    public function insertCustomer()
    {
        $this->setData();

        $compare = $this->compareCustomer();

        if ($compare != false) {
            $model = Customer::findOne($compare);
            $model->email_user = $this->email_user;
            $model->phone = $this->phone;
            $model->country_id = $this->country_id;
            $model->city_id = $this->city_id;
            $model->birth_date = $this->birth_date;
            $model->setAddress();
            return ['update', $model->id];
        } else {
            $this->save();
            return ['create', $this->id];
        }

    }

    public function compareCustomer()
    {
        $data = $this->getCustomerData();

        if (!empty($data['full_name'])) {
            $resFull = [];
            foreach ($data['full_name'] as $key => $value) {

                if ($value['gender_id'] != $this->gender_id) continue;

                if ($value['first_name'] == $this->first_name && $value['last_name'] == $this->last_name) return $value['id'];

                similar_text($value['first_name'], $this->first_name, $SynonymFirstName);
                similar_text($value['last_name'], $this->last_name, $SynonymLastName);


                if (($SynonymFirstName == 100 && $SynonymLastName >= 90) || ($SynonymLastName == 100 && $SynonymFirstName >= 90)) {
                    if (!empty($resFull)) {
                        if ($SynonymFirstName < $resFull['first_name'] && $SynonymLastName < $resFull['last_name']) {
                            continue;
                        }
                    }

                    $resFull = [
                        'id' => $value['id'],
                        'first_name' => $SynonymFirstName,
                        'last_name' => $SynonymLastName,
                    ];
                }
            }

            if (!empty($resFull)) return $resFull['id'];
        }

        if (!empty($data['metaphone_full_name'])) {
            $resFull = [];
            foreach ($data['metaphone_full_name'] as $key => $value) {

                if ($value['gender_id'] != $this->gender_id) continue;

                if ($value['metaphone_first_name'] == $this->metaphone_first_name && $value['metaphone_last_name'] == $this->metaphone_last_name) return $value['id'];

                similar_text($value['metaphone_first_name'], $this->metaphone_first_name, $SynonymFirstName);
                similar_text($value['metaphone_last_name'], $this->metaphone_last_name, $SynonymLastName);


                if (($SynonymFirstName == 100 && $SynonymLastName >= 85) || ($SynonymLastName == 100 && $SynonymFirstName >= 85)) {
                    if (!empty($resFull)) {
                        if ($SynonymFirstName < $resFull['first_name'] && $SynonymLastName < $resFull['last_name']) {
                            continue;
                        }
                    }

                    $resFull = [
                        'id' => $value['id'],
                        'first_name' => $SynonymFirstName,
                        'last_name' => $SynonymLastName,
                    ];
                }
            }

            if (!empty($resFull)) return $resFull['id'];
        }

        return false;
    }


    private function getCustomerData()
    {
        $result['full_name'] = Customer::find()
            ->where(['first_name' => $this->first_name])
            ->orWhere(['last_name' => $this->first_name])
            ->asArray()->all();
        $result['metaphone_full_name'] = Customer::find()
            ->where(['like', 'metaphone_first_name', $this->metaphone_first_name])
            ->where(['like', 'metaphone_last_name', $this->metaphone_last_name])
            ->asArray()->all();

        return $result;
    }


}
