<?php

namespace app\models\search;

use app\components\MyHelper;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Customer;

/**
 * CustomerSearch represents the model behind the search form of `app\models\Customer`.
 */
class CustomerSearch extends Customer
{

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'gender_id', 'country_id', 'city_id', 'phone'], 'integer'],
            [['first_name', 'last_name', 'birth_date', 'email_user', 'metaphone_first_name', 'metaphone_last_name', 'created_at', 'updated_at', 'qty_orders', 'total_income'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Customer::find()->alias('c');

        $query->joinWith('customerAddress ca');
//        $query->joinWith('customerAddressOne cao');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'c.id' => $this->id,
            'c.gender_id' => $this->gender_id,
            'ca.country_id' => $this->country_id,
            'ca.city_id' => $this->city_id,
            'c.birth_date' => $this->birth_date,


        ]);

        $query->andFilterWhere(['like', 'c.first_name', $this->first_name])
            ->andFilterWhere(['like', 'c.last_name', $this->last_name])
            ->orFilterWhere(['like', 'c.metaphone_first_name', metaphone(MyHelper::translit($this->first_name))])
            ->orFilterWhere(['like', 'c.metaphone_last_name', metaphone(MyHelper::translit($this->last_name))]);

        $query->groupBy('c.id');

        return $dataProvider;
    }
}
