<?php

namespace app\models;

/**
 * This is the model class for table "customer".
 *
 * @property int $$idNewUser
 * @property int $idOldUser
 */
class UpdateCustomer
{
    public $idNewUser;
    public $idOldUser;
    public $model;



    public function start()
    {
        $this->model = Customer::findOne($this->idNewUser);
        $this->addOrders();
        $this->addAddress();
        $this->delete();
    }

    private function addOrders()
    {
        Order::updateAll(['customer_id' => $this->idNewUser], ['customer_id' => $this->idOldUser]);

    }
    private function addAddress()
    {
        CustomerAddress::updateAll(['customer_id' => $this->idNewUser], ['customer_id' => $this->idOldUser]);
    }
    private function delete()
    {
        Customer::findOne($this->idOldUser)->delete();
    }
}
