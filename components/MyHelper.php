<?php


namespace app\components;

use common\models\Setting;
use yii\base\Model;
use Yii;

class MyHelper extends Model
{

    public static function translit($data)
    {
        $rus = [
            'а', 'б', 'в', 'г', 'д', 'е', 'ё',
            'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п',
            'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ',
            'ъ', 'ы', 'ь', 'э', 'ю', 'я', 'ї', 'є', 'і', 'ґ'
        ];
        $lat = [
            'a', 'b', 'v', 'g', 'd', 'e','e',
            'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o','p',
            'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch',
            'y', 'y', 'y', 'e', 'yu', 'ya', 'i', 'e', 'i', 'g'
        ];

        return str_replace($rus, $lat, preg_replace(["#\s\s*#ui", "#\"+#", "#[^\d\w]#ui", "#\"+#"], "", trim(mb_strtolower($data))));
    }

    /**
     * @inheritdoc
     * function Array Help
     *
     * @param array $array
     * @param string $key
     * @param array $values
     * @param array $result
     *
     * $this->arrayHelp($results, 'id', ['first_name', 'last_model', 'year']);
     *
     */
    public static function arrayHelp($array, $key, $values = [])
    {
        $result = [];
        foreach ($array as $params){
            $name = '';
            $i = 0;
            foreach ($values as $value){
                $name .= $i == 0 ? $params[$value] : ' ' . $params[$value];
                $i++;
            }
            $result[$params[$key]] = $name;
        }

        return $result;
    }

    public static function phoneFormatter($data)
    {
        $pattern = "#^((\d){2})((\d){3})((\d){3})((\d){2})((\d){2})$#";
        $replace = "+$1($3)-$5-$7-$9";

        return preg_replace($pattern, $replace, $data);
    }




}